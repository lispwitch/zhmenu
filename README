	zhmenu
	adaptable pinyin-to-chinese input menu
	version 0.2

WHAT IS IT?
	zhmenu is an adaptable selective menu for the X Window System. The main
	utility of it is to provide a pinyin input method, however the database
	may be swapped out for other databases, and the run script altered to
	serve as a dmenu-like program.

	If that doesn't make much sense, you might want to try just running it.

HOW DO I RUN IT?
	zhmenu_run [options]

HOW DO I USE IT?
	Run it and start typing. To select an item use the arrow keys, to add
	the selection to the list press space. When you are comfortable with the
	selection list and the current item selected, press enter. This will
	cause zhmenu to exit, and write the selected items to stdout. The run
	script will then use xsel to copy these to your clipboard so you can
	paste it out. To de-select an item press the Home key, which will
	return you to the start of the selection list. To erase a character
	press the backspace button. If you have made at least one selection
	and you hit the backspace key when the current match string is empty,
	you will move to the previously selected item.

	The following keys are defined for use:

	escape		exit without doing anything

	backspace	remove a character from the current input
	                if input is empty, navigate to previously selcted item

	left arrow	move left in the selection list; if at the beginning
	                of the list, stop selecting anything

	right arrow	move right in the selection list

	spacebar	select the current item and move to the next item in
	                the selection list

	enter/return	output all selected items and exit

	home		stop selecting anything (move to the start of the list)


	zhmenu's database uses a TSV format of the following:

	<selection item>:tab:<match string><tab><extra data><newline>

	the input is matched against the match string, and will add the
	selection item to the list to output.

BUILDING
	To build zhmenu, run `mk`.

	A pinyin-chinese dataset generated from the Unicode Unihan database
	is provided. To regenerate this, run `mk data`. Please note that it is
	normal for it to run for around 20 minutes or so, and acquisition of
	the source files may require an internet connection.

INSTALLATION
	mk install [PREFIX=...] [BINDIR=...] [DBDIR=...]
	If you alter DBDIR, you will want to alter config.h / config.def.h so
	things match up.
	By default:
		PREFIX=/usr/local
		BINDIR=$PREFIX/bin/
		DBDIR=$PREFIX/share/zhmenu/

SETTINGS
	The fonts, colorscheme, database path, padding, and some miscellaneous
	options may be altered at runtime with a flag (`zhmenu -h` or
	`man zhmenu`for the full list, or the default can be set by editing
	`config.h`/`config.def.h`, and then rebuilding zhmenu.

LIBRARIES / DEPENDENCIES
	Building zhmenu requires you to have gcc, mk[0], X11, Xft, and
	fontconfig. Running zhmenu requires xsel.

	If you do not have Noto Sans CJK Simplified Chinese Regular installed,
	you may either wish to install it or you may wish to change the font,
	see SETTINGS for more information.

	Linting requires scan-build, sparse, and cppcheck to be installed.

	Building the chinese-pinyin database relies on wget, grep, awk, sed,
	sort, and bash to be installed.

	[0]: https://9fans.github.io/plan9port/unix/

LICENSE
	BSD 3-Clause Clear License (See LICENSE).

	Except for:

	data/sc-pinyin.txt
		In the case that the license for the Unicode data files[1] does
		not apply because the file is a derivative work, it shall be
		licensed under CC BY-NC-SA 4.0

CONTACT / BUGS / FEATURES
	Please please *please* report any bugs to alexandria@inventati.org
	As of yet there are no known bugs.
	Planned features are:

	* A flag to show an extra line displaying the current accumulated
	selection so far, along with the extra data of the currently selected
	element
