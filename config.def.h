/* Xft font string */
static char *fontstr = "NotoSansCJKsc-Regular:size=12";

/* Directories to search for the db file to use */
/* You can specify a specific file at runtime with a flag, if you must */
static char *dbpath = "/usr/local/share/zhmenu/sc-pinyin.txt:data/sc-pinyin.txt";

/* blatantly stolen from dmenu's colorscheme */
static char *colorscheme[SCM_NUMITEMS] = {
	[SCM_WINDOWBACK] = "#222222",
	[SCM_WINDOWFORE] = "#bbbbbb",
	[SCM_SELECTBACK] = "#005577",
	[SCM_SELECTFORE] = "#eeeeee"
};

/* Padding for menu items, in pixels */
static int item_padwidth = 4;
static char *item_leftarrow = "<";
static char *item_rightarrow = ">";
