CC=cc
CFLAGS= -std=c99 -pedantic -Wall -Os -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -fstack-protector-strong -fno-omit-frame-pointer # -fsanitize=undefined
CINC=  -I/usr/X11R6/include -I/usr/include/freetype2 
CLIB= -L/usr/X11R6/lib -lX11 -lfontconfig -lXft 

SCAN=scan-build-7
SCANFLAGS= -enable-checker core -enable-checker security -enable-checker unix

SPARSEFLAGS= -Wsparse-all -gcc-base-dir `gcc --print-file-name=`
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all --suppress=missingIncludeSystem

DATAURL="https://www.unicode.org/Public/UCD/latest/ucd/Unihan.zip"
DATANAME="Unihan.zip"
TARGET= bin/zhmenu
DB= sc-pinyin.txt
FILES= `{ls *.c}
MAN= zhmenu.1

PREFIX=/usr/local
BINDIR=$PREFIX/bin/
DBDIR=$PREFIX/share/zhmenu/
MANDIR=$PREFIX/man/man1/

build:V: $FILES
  [ -d bin ] || mkdir bin
  [ -e config.h ] || cp config.def.h config.h
  $CC $CFLAGS $FILES -o $TARGET $CINC $CLIB

lint:V: $FILES
  [ -d bin ] || mkdir bin
  [ -e config.h ] || cp config.def.h config.h
  echo ":::: Sparse output: ::::" && sparse $SPARSEFLAGS $CINC $FILES
  echo ":::: Cppchk output: ::::" && cppcheck $CPPCHKFLAGS $FILES
  $SCAN $SCANFLAGS $CC -g $CFLAGS $FILES -o $TARGET $CINC $CLIB

getdata:V:
  cd data
  [ -e $DATANAME ] || ( wget $DATAURL )

data:V: getdata
  cd data
  [ -z "Unihan_*" ] || rm -f Unihan_*
  unzip -q $DATANAME
  ./make.sh $DB
  rm -f Unihan_*

clean:
  rm -f *.o
  rm -rf bin

nuke: clean
  rm -rf data/sc-pinyin.txt
  rm -f data/Unihan.zip

test: build
  ./$TARGET

install:V:
  # Move the binaries
  mkdir -p $BINDIR
  cp -f $TARGET $BINDIR
  chmod 755 $BINDIR/zhmenu
  cp -f zhmenu_run $BINDIR
  chmod 755 $BINDIR/zhmenu_run
  # Move the database
  mkdir -p $DBDIR
  cp -f data/$DB $DBDIR
  chmod 644 $DBDIR/$DB
  # Move the manual
  mkdir -p $MANDIR
  cp -f $MAN $MANDIR
  chmod 644 $MANDIR/$MAN

uninstall:V:
  rm -f $BINDIR/zhmenu
  rm -f $DBDIR/$DB
  rm -f $MANDIR/$MAN
  rmdir $DBDIR
